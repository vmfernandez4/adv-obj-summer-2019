/**
 *
 */
package homework1;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/** This class is will model a tetromino.
 * @author epadilla2
 * @param <U>
 * @param <T>
 *
 */
public class Tetromino {

    /**
     * Rotates tetromino to the right.
     */
    public void rotateRight() {
        //your code goes here
    }

    /**
     * Rotates tetromino to the left.
     */
    public void rotateLeft() {
        //your code goes here
    }

    /**
     * Enumeration to list the different tetrominos.
     * FILLER used when in multiplayer sending a line to the other player.
     * @author epadilla2
     *
     */
    public enum TetrominoEnum {
        /** Types of tetrominos, filler represents punishment lines added in multiplayer mode */

        I(0), J(1), L(2), O(3), S(4), Z(5), T(6), FILLER(7);
        /** Integer value of each tetromino*/
        private int value;
        /**  Hash for inverse lookup of a tetromino based on value*/
        private static final Map<Integer, TetrominoEnum> reverseLookup = new HashMap<Integer, TetrominoEnum>();

        static {
            for (TetrominoEnum tetromino : TetrominoEnum.values()) {
                reverseLookup.put(tetromino.getValue(), tetromino);
            }
        }

        /**
         * Constructor that sets the integer value of tetromino
         * @param value
         */
        TetrominoEnum(int value) {
            this.value = value;
        }

        /**
         * Return integer value of tetromino
         * @return
         */
        public int getValue() {
            return value;
        }

        /**
         * Return TetrominoEnum depending on value
         * @param value
         * @return
         */
        public static TetrominoEnum getEnumByValue(int value) {
            return reverseLookup.get(value);
        }

        /**
         * Returns a random TetrominoEnum
         * @return
         */
        public static TetrominoEnum getRandomTetromino() {
            Random random = new Random();
            return values()[random.nextInt(values().length - 1)];
        }

        public void createFig(){
            int[][] IFig = new int[][]{
                    {0, 1, 0, 0},
                    {0, 1, 0, 0},
                    {0, 1, 0, 0},
                    {0, 1, 0, 0}
            };

            int[][] JFig = new int[][]{
                    {0, 1, 0},
                    {0, 1, 0},
                    {1, 1, 0},
            };

            int[][] LFig = new int[][]{
                    {0, 1, 0},
                    {0, 1, 0},
                    {0, 1, 1},
            };

            int[][] OFig = new int[][]{
                    {0, 0, 0, 0},
                    {0, 1, 1, 0},
                    {0, 1, 1, 0},
                    {0, 0, 0, 0}
            };

            int[][] SFig = new int[][]{
                    {0, 1, 1},
                    {1, 1, 0},
                    {0, 0, 0},
            };

            int[][] ZFig = new int[][]{
                    {1, 1, 0},
                    {0, 1, 1},
                    {0, 0, 0},
            };

            int[][] TFig = new int[][]{
                    {0, 0, 0},
                    {1, 1, 1},
                    {0, 1, 0},
            };

        }
    }
}
