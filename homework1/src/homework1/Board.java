package homework1;

/**
 * Model class from MVC. This class contains the Tretris logic but no GUI information.
 * Model must not know anything about the GUI and Controller.
 *
 * @author epadilla2
 * @author victor fernandez
 */
public class Board extends java.util.Observable {

    private int level;
    private int score;
    private boolean isGameActive;
    private int [][] grid;

    public Board() {
        this.isGameActive = true;
        this.grid = new int [19][9];
        for (int i = 0; i < 19; i++) {
            for (int j = 0; j < 9; j++) {
                this.grid[i][j] = 0;

            }

        }
    }

    /**
     * Returns if game is active
     *
     * @return
     */
    public boolean isGameActive() {
        return isGameActive;
    }

    /**
     * Returns score
     *
     * @return
     */
    public int getScore() {
        return score;
    }

    /**
     * Return current level
     *
     * @return
     */
    public int getLevel() {
        return level;
    }

    /**
     * Moves tetromino down
     */
    public void moveTetrominoDown() {
        //your code goes here
    }

    public void moveTetrominoRight() {
        //your code goes here
    }

    public void moveTetrominoLeft() {
        //your code goes here
    }

    /**
     * Returns if this is a valid position
     *
     * @return
     */
    private boolean validateTetrominoPosition() {
        //your code goes here
        return true;
    }

    public void print(){
        for (int i = 0; i < 19; i++) {
            System.out.print("|");
            for (int j = 0; j < 9; j++) {

                System.out.print(" " + this.grid[i][j] + " ");

            }
            System.out.println("|");
        }
    }

}
